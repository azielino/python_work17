import csv
import json
import pickle
import sys
import os

input_list = sys.argv[1:]
source = input_list[0]
destination = input_list[1]
changes = input_list[2:]


class Files:

    EXT = (
        'csv',
        'json',
        'pickle'
        )

    def __init__(self, src, dst):
        self.src = src
        self.dst = dst
        self.data_list = list()
        self.dir_path = os.getcwd()
        self.dir_files = os.listdir(path=f'{self.dir_path}')
        self.check = self.check_file(self.src)

    def source_files(self, extensions, files):
        for file in files:
            for text in extensions:
                if text in file:
                    yield file

    def dst_filetype(self):
        return self.dst.split(".")[-1]

    def check_file(self, file):
        if not os.path.exists(file):
            print(f"\nBlad! Plik zrodlowy nie istnieje.\n\nDostepne pliki to: ")
            for file in self.source_files(self.EXT, self.dir_files):
                print(file)
            return False
        return True

    def changed(self, rows):
        for word in changes:
            word = word.split(",")
            y = int(word[0])
            x = int(word[1])
            value = word[2]
            rows[y][x] = value
        return rows

    def create_csv_dst(self, name):
        print(f"\nZawartosc pliku {name}:\n")
        with open(F"{name}", "w", newline="") as file:
            written_csv = csv.writer(file)
            for row in self.data_list:
                written_csv.writerow(row)
                print(row)

    def create_json_dst(self, name):
        print(f"\nZawartosc pliku {name}:\n")
        with open(F"{name}", "w") as file:
            for row in self.data_list:
                print(row)
            json.dump(self.data_list, file)

    def create_pickle_dst(self, name):
        print(f"\nZawartosc pliku {name}:\n")
        with open(F"{name}", "wb") as file:
            for row in self.data_list:
                print(row)
            pickle.dump(self.data_list, file)


class FileCsv(Files):

    def change_source(self):
        with open(f"{source}") as file:
            readed = csv.reader(file)
            for line in readed:
                self.data_list.append(line)
            try:
                self.data_list = self.changed(self.data_list)
                getattr(obj, f'create_{self.dst_filetype()}_dst')(self.dst)
            except IndexError:
                print('\nZle wartosci dla zmian')
            except AttributeError:
                print('\nZly typ pliku docelowego')


class FileJson(Files):

    def change_source(self):
        with open(f"{source}", "r") as file:
            readed = json.load(file)
            if type(readed) == type({}):
                for key, value in readed.items():
                    self.data_list.append([key, value])
            else:
                for key, value in readed:
                    self.data_list.append([key, value])
            try:
                self.data_list = self.changed(self.data_list)
                getattr(obj, f'create_{self.dst_filetype()}_dst')(self.dst)
            except IndexError:
                print('\nZle wartosci dla zmian')
            except AttributeError:
                print('\nZly typ pliku docelowego')


class FilePickle(Files):

    def change_source(self):
        with open(f"{source}", "rb") as file:
            readed = pickle.load(file)
            if type(readed) == type({}):
                for key, value in readed.items():
                    self.data_list.append([key, value])
            else:
                for key, value in readed:
                    self.data_list.append([key, value])
            try:
                self.data_list = self.changed(self.data_list)
                getattr(obj, f'create_{self.dst_filetype()}_dst')(self.dst)
            except IndexError:
                print('\nZle wartosci dla zmian')
            except AttributeError:
                print('\nZly typ pliku docelowego')


obj = False
if '.csv' in source:
    obj = FileCsv(source, destination)
elif '.json' in source:
    obj = FileJson(source, destination)
elif '.pickle' in source:
    obj = FilePickle(source, destination)
else:
    print('Plik zrodlowy ma zle rozszerzenie')
if obj and obj.check:
        obj.change_source()